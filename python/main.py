import json
import socket
import sys
import math

class Brain(object):
    def __init__(self, game_init_data):
        self.data = game_init_data

class Piece(object):
    def __init__(self, piece_data, track):
        self.track = track

        try:
            self.radius = piece_data['radius']
            self.angle = piece_data['angle']
            self.bend = True
            self.max_speed = min(10, 4 + (((self.radius - 50) / 50) * 3))
        except KeyError:
            self.radius = float("inf")
            self.angle = 0
            self.straight_length = piece_data['length']
            self.bend = False

        try:
            self.switch = piece_data['switch']
        except KeyError:
            self.switch = False

        self.length = {}
        for index in self.track.lanes:
            self.length[index] = self.calc_length(index)


    def calc_length(self, start_lane, end_lane=None):
        if end_lane is None:
            end_lane = start_lane

        lane_diff = abs(self.track.lanes[start_lane] - self.track.lanes[end_lane])

        if self.bend:
            if self.angle > 0:
                flip = -1
            else:
                flip = 1

            avg_distance_from_center = (self.track.lanes[start_lane] + self.track.lanes[end_lane]) / 2.0
            return (abs(self.angle) / 360.0) * 2 * math.pi * (self.radius + (flip * avg_distance_from_center))
        else:
            return math.sqrt(self.straight_length ** 2 + lane_diff ** 2)

    def next(self):
        return self.track.get_piece(self, 1)

    def previous(self):
        return self.track.get_piece(self, -1)

    def same_curve(self, other):
        if self.radius == other.radius:
            if self.angle >= 0 and other.angle >= 0:
                return True
            if self.angle <= 0 and other.angle <= 0:
                return True
        return False


class Track(object):
    def __init__(self, game_init_data):
        track = game_init_data['race']['track']

        self.name = track['name']
        self.id = track['id']

        self.lanes = {}
        for lane in track['lanes']:
            self.lanes[lane['index']] = lane['distanceFromCenter']

        self.pieces = []
        for piece in track['pieces']:
            self.pieces.append(Piece(piece, self))


    def get_piece(self, piece, index_modifier=0):
        index = self.pieces.index(piece) + index_modifier
        if index < 0:
            index += len(self.pieces)
        if index >= len(self.pieces):
            index %= len(self.pieces)
        return self.pieces[index]

    def get_pieces(self, piece, step=1):
        index = self.pieces.index(piece) 
        return self.pieces[index::step] + self.pieces[:index:step]

class Position(object):
    def __init__(self, track, position, last_position=None):
        self.track = track
        if last_position is None:
            last_position = self
        self.last_position = last_position

        self.angle = position['angle']

        piece_position = position['piecePosition']
        self.in_piece_distance = piece_position['inPieceDistance']
        self.piece = track.pieces[piece_position['pieceIndex']]
        self.start_lane = piece_position['lane']['startLaneIndex']
        self.end_lane = piece_position['lane']['endLaneIndex']

        self.speed = self.calc_speed()
        self.throttle = None

    def new(self, position):
        return Position(self.track, position, self)


    def calc_speed(self):
        last_distance = self.last_position.in_piece_distance
        if self.in_piece_distance >= last_distance:
            return self.in_piece_distance - last_distance
        else:
            last_lane = self.last_position.end_lane
            return self.piece.previous().length[last_lane] - last_distance + self.in_piece_distance

    def piece_length(self):
        return self.piece.calc_length(self.start_lane, self.end_lane)

class VroomBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.game_tick = -1
        self.compound_error = 0
        self.turbo_available = False
        self.lap = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    def join_track(self, track):
        return self.msg("joinRace", {
                                        "botId": {
                                            "name": self.name,
                                            "key": self.key
                                        },
                                        "trackName": track,
                                        "carCount": 1
                                    })

    def throttle(self, throttle):
        if throttle > 1.0:
            throttle = 1.0
        if throttle < 0.0:
            throttle = 0.0

        self.position.throttle = throttle
        self.msg("throttle", throttle)

    def switch(self, side):
        self.msg("switchLane", side)

    def turbo(self):
        self.msg("turbo", "Go Vroom")

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        #self.join_track("usa")
        #self.join_track("germany")
        #self.join_track("france")
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        self.lap = 0
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        for car_pos in data:
            if car_pos['id']['name'] == self.car['name'] and car_pos['id']['color'] == self.car['color']:
                try:
                    self.position = self.position.new(car_pos)
                except AttributeError:
                    self.position = Position(self.track, car_pos)
                break
        if self.qualifying:
            self.learnFromPreviousBend()

        if self.useTurboOnLongStraight():
            return
        if self.takeShortestRoute():
            return
        if self.dontSlowDownAtTheEnd():
            return
        if self.slowDownForCorners():
            return
        if self.driftToTheMaxInCorners():
            return

        self.throttle(1.0)

    def learnFromPreviousBend(self):
        if (self.position.speed + self.position.in_piece_distance) > self.position.piece_length():
            if self.position.piece.bend:
                position = self.position
                max_angle = 0
                while position.piece.same_curve(position.last_position.piece) and not position == position.last_position:
                    max_angle = max(max_angle, abs(position.angle))
                    position = position.last_position

                position = position.last_position
                slowed_down = False
                while position.piece == position.last_position.piece and not position == position.last_position:
                    if position.throttle == 0:
                        slowed_down = True
                    position = position.last_position

                if not slowed_down:
                    return

                
                if max_angle < 54:
                    self.position.piece.max_speed *= (1 + ((54 - max_angle) / 540.0))
                    print "Setting %s as max_speed" % (self.position.piece.max_speed)

                    bend_segment = self.track.get_pieces(self.position.piece, -1)

                    for piece in bend_segment:
                        if piece.bend:
                            if piece.radius == self.position.piece.radius:
                                if piece.angle > 0 and self.position.piece.angle > 0:
                                    continue
                                if piece.angle < 0 and self.position.piece.angle < 0:
                                    continue
                        bend_segment = bend_segment[:bend_segment.index(piece)] 
                        break

                    actual_max_speed = min([p.max_speed for p in bend_segment])

                    for p in bend_segment:
                        p.max_speed = actual_max_speed
                    print "Adjusted max_speed for %s pieces" % (len(bend_segment))


    def dontSlowDownAtTheEnd(self):
        if not self.qualifying:
            if self.lap == self.total_laps:
                remaining_track = self.track.pieces[self.track.pieces.index(self.position.piece):]
    
                if all(not piece.bend for piece in remaining_track):
                    self.throttle(1.0)
                    return True

        return False


    def useTurboOnLongStraight(self):
        if self.turbo_available:
            remaining_track = self.track.pieces[self.track.pieces.index(self.position.piece):]
    
            track_pieces = self.track.get_pieces(self.position.piece)

            straights = 0
            for piece in track_pieces:
                if piece.bend:
                    break
                straights += 1

            if straights >= 5:
                self.turbo()
                print self.game_tick, "Turbo"
                self.turbo_available = False
                return True
        return False

    def takeShortestRoute(self):
        if (self.position.speed * 2 + self.position.in_piece_distance) > self.position.piece_length() \
            and (self.position.speed + self.position.in_piece_distance) < self.position.piece_length():


            if self.position.piece.next().switch:
                current_lane = self.position.end_lane
                total_length = {}
                for lane in self.track.lanes:
                    piece = self.position.piece.next()
                    total_length[lane] = piece.calc_length(current_lane, lane)
                    piece = piece.next()
                    while piece.switch is not True:
                        total_length[lane] += piece.length[lane]
                        piece = piece.next()
                    

                target = min(total_length, key=total_length.get)
                if target > current_lane:
                    self.switch('Right')
                    #print self.game_tick, "Right", target, current_lane, total_length, self.position.speed
                    return True
                if target < current_lane:
                    self.switch('Left')
                    #print self.game_tick, "Left", target, current_lane, total_length, self.position.speed
                    return True

        return False

    def driftToTheMaxInCorners(self):
        if not self.position.piece.bend:
            return False

        if not self.position.piece.same_curve(self.position.last_position.piece):
            print "Reset compound_error"
            self.compound_error = 0

        if self.position.piece.angle > 0:
            flip = 1
        else:
            flip = -1

        sp = 54

        error = sp - (flip * self.position.angle)
        self.compound_error += error
        p = error / 60
        i = self.compound_error / 1800
        d = (error - (sp - (flip * self.position.last_position.angle))) / 4

        throttle = p + i + d
    
        self.throttle(throttle)
        print "%s, %s, %s, %s, %s, %s, %s, %s" % (self.game_tick, throttle, self.position.speed, self.position.angle, error, p, i, d)
        return True
    
    def slowDownForCorners(self):
        length_until_bend = self.position.piece_length() - self.position.in_piece_distance
        bend_piece = self.position.piece.next()
        while not bend_piece.bend:
            length_until_bend += bend_piece.length[self.position.end_lane]
            bend_piece = bend_piece.next()

        target_speed = bend_piece.max_speed

        ticks_until_bend = length_until_bend / ((self.position.speed + target_speed) / 2)

        if ticks_until_bend < ((self.position.speed - target_speed) / 0.15):
            print "%s, %s, %s" % (self.game_tick, self.position.speed, bend_piece.max_speed)
            self.throttle(0.0)
            return True

        return False

    def on_crash(self, data):
        print("Someone crashed")
        if data['name'] == self.car['name'] and data['color'] == self.car['color']:
            print("Reducing max_speed")
            pieces = self.track.get_pieces(self.position.piece, -1)
            try:
                self.position.piece.max_speed *= 0.9
                for piece in pieces[1:]:
                    if not piece.same_curve(piece.next()):
                        break
                    piece.max_speed *= 0.9
            except AttributeError:
                pass


    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        try:
            if not self.track.id == data['race']['track']['id']:
                self.track = Track(data)
        except AttributeError:
            self.track = Track(data)

        race_session = data['race']['raceSession']
        try:
            self.total_laps = race_session['laps']
            self.qualifying = False
        except KeyError:
            self.qualifying_duration = race_session['durationMs']
            self.qualifying = True

    def on_your_car(self, data):
        self.car = data

    def on_turbo_available(self, data):
        self.turbo_available = True

    def on_lap_finished(self, data):
        if data['car']['name'] == self.car['name'] and data['car']['color'] == self.car['color']:
            self.lap = data['raceTime']['laps'] + 1


    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
            'turboAvailable': self.on_turbo_available,
            'lapFinished': self.on_lap_finished,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            try:
                self.game_tick = msg['gameTick']
            except KeyError:
                pass

            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = VroomBot(s, name, key)
        bot.run()
